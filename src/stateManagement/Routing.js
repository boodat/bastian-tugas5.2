import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "./Login";
import Home from "./Home";
import AddData from "./AddData";

const Stack = createNativeStackNavigator();

export default function Routing ({Navigator, route}) {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Login" component={Login}/>
                <Stack.Screen name="Home Page" component={Home}/>
                <Stack.Screen name="Add Data" component={AddData}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}